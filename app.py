import json

from psycopg2.extras import NamedTupleCursor
import tornado.web
import tornado.ioloop
import tornado.options
import tornado.httpserver
from tornado import gen

import momoko

from engine import ParserSite

db_database = 'boston'
db_user = 'bostonandrewrds'
db_password = 'asjdh3465GfiKKKKdf345fg47236gfhj'
db_host = 'bostonrds.ck4hajxxmdn3.us-west-2.rds.amazonaws.com'
db_port = 5432
dsn = 'dbname=%s user=%s password=%s host=%s port=%s' % (
    db_database, db_user, db_password, db_host, db_port)

assert (db_database or db_user or db_password or db_host or db_port) is not None, (
    'Environment variables for the examples are not set. Please set the following '
    'variables: MOMOKO_TEST_DB, MOMOKO_TEST_USER, MOMOKO_TEST_PASSWORD, '
    'MOMOKO_TEST_HOST, MOMOKO_TEST_PORT')


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, expiry, uid,"
                                                        "if-modified-since, access-token, authorization,"
                                                        "client, Content-Type")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        # no body
        self.set_status(204)
        self.finish()


class OverviewHandler(BaseHandler):

    @gen.coroutine
    def get(self):
        connection = yield self.db.getconn()
        with self.db.manage(connection):
            print('pass')
            parser = ParserSite(6232, 1, self.db)
            parser.run()
            #parser = ParserSite(10, 'http://feeds.thekitchn.com/apartmenttherapy/thekitchn/', connection)
            #parser.run()
        self.write("""
<ul>
    <li><a href="/mogrify">Mogrify</a></li>
    <li><a href="/query">A single query</a></li>
    <li><a href="/hstore">A hstore query</a></li>
    <li><a href="/json">A JSON query</a></li>
    <li><a href="/transaction">A transaction</a></li>
    <li><a href="/multi_query">Multiple queries executed by yielding a list</a></li>
    <li><a href="/connection">Manual connection management</a></li>
</ul>
        """)
        self.finish()

    @gen.coroutine
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        print('POST', post)
        if 'Signature' in post:
            data = json.loads(post.get('Message', '{}'))
            parser = ParserSite(data.get('link_id'), data.get('site_id'), self.db)
            parser.run()


def main():
    print('start')
    try:
        tornado.options.parse_command_line()
        application = tornado.web.Application([
            (r'/', OverviewHandler),
        ], debug=True, xsrf_cookies=False,
                cookie_secret="6sdaf1ofETzKXQAGaYjddkL5gEmGeJJsdfFuYh7EQnp2XdTP1o/Vo=")

        ioloop = tornado.ioloop.IOLoop.instance()

        application.db = momoko.Pool(
                dsn=dsn,
                size=4,
                max_size=6,
                cursor_factory=NamedTupleCursor,
                ioloop=ioloop,
                setsession=("SET TIME ZONE UTC",),
                raise_connect_errors=False,

        )

        future = application.db.connect()
        ioloop.add_future(future, lambda f: ioloop.stop())
        ioloop.start()

        if application.db.server_version >= 90200:
            future = application.db.register_json()
            # This is the other way to run ioloop in sync
            ioloop.run_sync(lambda: future)

        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(8050)
        ioloop.start()
    except KeyboardInterrupt:
        print('Exit')


# if __name__ == '__main__':
main()
